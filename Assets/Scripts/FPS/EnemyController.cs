﻿using UnityEngine;

public class EnemyController : MonoBehaviour {

    private CharacterHealth characterHealth;
    public int points;

	// Use this for initialization
	void Start () {
        GameObject.Find("Enemy").GetComponent<MeshRenderer>().enabled = true;
        GameObject.Find("enemycollada-male_casualsuit05").GetComponent<MeshRenderer>().enabled = true;
        GameObject.Find("enemycollada-base").GetComponent<MeshRenderer>().enabled = true;
        characterHealth = GetComponent<CharacterHealth>();
	}
	
	// Update is called once per frame
	void Update () {
        if (characterHealth.isDead) {
            GameController.score+=points;
            Destroy(gameObject, 0.2f);
            enabled = false;
        }
	}
}
